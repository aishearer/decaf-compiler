#!/bin/bash
mkdir -p myout
make
scripts/maketest.sh
FILES=samples/*.out
for f in $FILES
do
	temp=$(basename $f)
	mo=${temp%%.*}
	echo "Processing $f and myout/$mo.frag files..."
	diff -w $f myout/$mo.frag
done
