#!/bin/bash

FILES=samples/*.frag
for f in $FILES
do
	echo "Processing $f file..."
	./dcc < $f &> myout/$(basename $f)
done
