CISC 471 Fall 2014
written by Andrew Shearer.

All sample files should be compiling and running correctly








I added shell scripts in the scripts directory that tests all samples with my compiler.
Use it by executing it from the same directory that has the make file.
Example usage:
scripts/testall.sh
scripts/superTest.sh

There are also scripts to compare the output of the solution dcc with my own
compiler.
Example usage:
scripts/comp.sh "test string"
scripts/compfile.sh samples/switch.decaf
