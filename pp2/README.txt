CISC 471 Fall 2014
Decaf parser written by Andrew Shearer.

I added shell scripts in the scripts directory that tests all samples with my parser.
Use it by executing it from the same directory that has the make file.
Example usage:
scripts/testall.sh

There are also scripts to compare the output of the solution dcc with my own
parser.
Example usage:
scripts/comp.sh "test string"
scripts/compfile.sh samples/switch.decaf
