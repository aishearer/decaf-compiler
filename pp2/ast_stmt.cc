/* File: ast_stmt.cc
 * -----------------
 * Implementation of statement node classes.
 */
#include "ast_stmt.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "ast_expr.h"


Program::Program(List<Decl*> *d) {
    Assert(d != NULL);
    (decls=d)->SetParentAll(this);
}

void Program::PrintChildren(int indentLevel) {
    decls->PrintAll(indentLevel+1);
    printf("\n");
}

StmtBlock::StmtBlock(List<VarDecl*> *d, List<Stmt*> *s) {
    Assert(d != NULL && s != NULL);
    (decls=d)->SetParentAll(this);
    (stmts=s)->SetParentAll(this);
}

void StmtBlock::PrintChildren(int indentLevel) {
    decls->PrintAll(indentLevel+1);
    stmts->PrintAll(indentLevel+1);
}

CaseStmt::CaseStmt(IntConstant *caseNum, List<Stmt*> *stmtList) {
    Assert(caseNum != NULL);
    this->caseNum = caseNum;
    this->stmtList = stmtList;  // can be null
    caseNum->SetParent(this);
    if (stmtList) stmtList->SetParentAll(this);
}

void CaseStmt::PrintChildren(int indentLevel) {
    caseNum->Print(indentLevel+1);
    if (stmtList) stmtList->PrintAll(indentLevel+1);
}

DefaultStmt::DefaultStmt(List<Stmt*> *stmtList) {
    this->stmtList = stmtList;  // can be null
    if (stmtList) stmtList->SetParentAll(this);
}

void DefaultStmt::PrintChildren(int indentLevel) {
    if (stmtList) stmtList->PrintAll(indentLevel+1);
}

SwitchStmt::SwitchStmt(Expr *field, List<Stmt*> *caseList, Stmt *defaultCase) {
    Assert(field != NULL && caseList != NULL); // defaultCase can be null
    this->field = field;
    this->caseList = caseList;
    this->defaultCase = defaultCase;	
    field->SetParent(this);
    caseList->SetParentAll(this);
    if (defaultCase) defaultCase->SetParent(this);
}

void SwitchStmt::PrintChildren(int identLevel) {
    field->Print(identLevel+1);
    caseList->PrintAll(identLevel+1);
    if (defaultCase) defaultCase->Print(identLevel+1);
}

ConditionalStmt::ConditionalStmt(Expr *t, Stmt *b) { 
    Assert(t != NULL && b != NULL);
    (test=t)->SetParent(this); 
    (body=b)->SetParent(this);
}

ForStmt::ForStmt(Expr *i, Expr *t, Expr *s, Stmt *b): LoopStmt(t, b) { 
    Assert(i != NULL && t != NULL && s != NULL && b != NULL);
    (init=i)->SetParent(this);
    (step=s)->SetParent(this);
}

void ForStmt::PrintChildren(int indentLevel) {
    init->Print(indentLevel+1, "(init) ");
    test->Print(indentLevel+1, "(test) ");
    step->Print(indentLevel+1, "(step) ");
    body->Print(indentLevel+1, "(body) ");
}

void WhileStmt::PrintChildren(int indentLevel) {
    test->Print(indentLevel+1, "(test) ");
    body->Print(indentLevel+1, "(body) ");
}

IfStmt::IfStmt(Expr *t, Stmt *tb, Stmt *eb): ConditionalStmt(t, tb) { 
    Assert(t != NULL && tb != NULL); // else can be NULL
    elseBody = eb;
    if (elseBody) elseBody->SetParent(this);
}

void IfStmt::PrintChildren(int indentLevel) {
    test->Print(indentLevel+1, "(test) ");
    body->Print(indentLevel+1, "(then) ");
    if (elseBody) elseBody->Print(indentLevel+1, "(else) ");
}


ReturnStmt::ReturnStmt(yyltype loc, Expr *e) : Stmt(loc) { 
    Assert(e != NULL);
    (expr=e)->SetParent(this);
}

void ReturnStmt::PrintChildren(int indentLevel) {
    expr->Print(indentLevel+1);
}
  
PrintStmt::PrintStmt(List<Expr*> *a) {    
    Assert(a != NULL);
    (args=a)->SetParentAll(this);
}

void PrintStmt::PrintChildren(int indentLevel) {
    args->PrintAll(indentLevel+1, "(args) ");
}


