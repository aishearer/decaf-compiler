CISC 471 Fall 2014
written by Andrew Shearer.

Node that in samples/bad6.decaf I report the error on line 4 before the error on line 11 and that the solution does the
reverse.  However, I also incorrectly reverse the errors for solution/bad3.decaf.  This is due to me checking class
declarations for undeclared types before global functions.


I added shell scripts in the scripts directory that tests all samples with my compiler.
Use it by executing it from the same directory that has the make file.
Example usage:
scripts/testall.sh

There are also scripts to compare the output of the solution dcc with my own
compiler.
Example usage:
scripts/comp.sh "test string"
scripts/compfile.sh samples/switch.decaf
