#!/bin/bash

FILES=samples/*.decaf
for f in $FILES
do
	echo "Processing $f file..."
	./dcc < $f &> myout/$(basename $f)
done
