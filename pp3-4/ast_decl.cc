/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "errors.h"
#include <typeinfo>
        
         
Decl::Decl(Identifier *n) : Node(*n->GetLocation()) {
    Assert(n != NULL);
    (id=n)->SetParent(this); 
}

void Decl::Check(Hashtable<Node*> *table, int pass) {
}

VarDecl::VarDecl(Identifier *n, Type *t) : Decl(n) {
    Assert(n != NULL && t != NULL);
    (type=t)->SetParent(this);
}

void VarDecl::Check(Hashtable<Node*> *table, int pass) {
    if (pass == 0) {
        type->Check(table, pass);
        astTable = table;
        bool inserted = table->insert(id->name, this); 
        if (!inserted) {
            ReportError::DeclConflict(this, dynamic_cast<Decl*>(table->stMap[id->name])); 
        }
    } else if (pass == 2) {
        type->Check(table, pass);
    }
}

NamedType* VarDecl::getClassName() {
   return dynamic_cast<NamedType*>(type);
}

ClassDecl::ClassDecl(Identifier *n, NamedType *ex, List<NamedType*> *imp, List<Decl*> *m) : Decl(n) {
    // extends can be NULL, impl & mem may be empty lists but cannot be NULL
    Assert(n != NULL && imp != NULL && m != NULL);     
    extends = ex;
    type = new NamedType(n);
    if (extends) extends->SetParent(this);
    (implements=imp)->SetParentAll(this);
    (members=m)->SetParentAll(this);
}

void ClassDecl::Check(Hashtable<Node*> *table, int pass) {
    if (pass == 0) {
        bool inserted = table->stMap.insert(std::make_pair(id->name, this)).second; 
        if (!inserted) {
            ReportError::DeclConflict(this, dynamic_cast<Decl*>(table->stMap[id->name])); 
        }
        Hashtable<Node*> *classTable = new Hashtable<Node*>(table);
        astTable = classTable;
        classTable->insert("this", this);
        
        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(classTable, pass);
        } 
    } else if (pass == 1) {
        // check if overriding signature matches
        if (extends) {
            Node* parentClass = table->stMap[extends->id->name];
            if (parentClass) {
                astTable->parent = parentClass->astTable;
            } else {
                ReportError::IdentifierNotDeclared(extends->id, LookingForClass); 
            }
        }
        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(table, pass);
        } 
    } else if (pass == 2) {
        for (int i = 0; i < implements->NumElements(); i++) {
            if (dynamic_cast<InterfaceDecl*>(table->get(implements->Nth(i)->id->name)) == NULL) {
                ReportError::IdentifierNotDeclared(implements->Nth(i)->id, LookingForInterface); 
                continue;
            }
            // check to see if this class implements all functions of the interface
            InterfaceDecl* interface = dynamic_cast<InterfaceDecl*>(table->stMap[implements->Nth(i)->id->name]);
            if (interface) {
                this->Implementer(interface);
            }
        }
        //check vardecls in superclasses
        for (int i = 0; i < members->NumElements(); i++) {
            VarDecl* vd = dynamic_cast<VarDecl*>(members->Nth(i));
            if (!vd) continue;
            Node* otherVar = NULL;
            Hashtable<Node*>* current = astTable->parent;
            while (current->parent != 0) {
                if (current->localDefined(vd->id->name)) {
                    otherVar = current->stMap[vd->id->name];
                    ReportError::DeclConflict(vd, dynamic_cast<Decl*>(otherVar)); 
                }
                current = current->parent; //put here so you don't check global functions
            }
        }
        if (extends && dynamic_cast<ClassDecl*>(table->get(extends->id->name)) != NULL) {
            for (int i = 0; i < members->NumElements(); i++) {
                FnDecl* fn = dynamic_cast<FnDecl*>(members->Nth(i));
                if (!fn) continue;
                Node* otherFunction = NULL;
                Hashtable<Node*>* current = astTable->parent; //get superclass's hashtable
                if (current->localDefined(fn->id->name)) {
                    otherFunction = current->stMap[fn->id->name];
                }
                while (current->parent != 0 && otherFunction == NULL) {
                    if (current->localDefined(fn->id->name)) {
                        otherFunction = current->stMap[fn->id->name];
                    }
                    current = current->parent; //put here so you don't check global functions
                }
                if (otherFunction) {
                    FnDecl* otherFn = dynamic_cast<FnDecl*>(otherFunction);
                    if (!otherFn) continue;
                    if (!fn->Equals(otherFn)) {
                        ReportError::OverrideMismatch(fn);
                    }
                }
            }
        }
        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(table, pass);
        }
    }
}

void ClassDecl::Implementer(InterfaceDecl* interface) {
    bool reportedNotImplemented = false;
    for (int i = 0; i < interface->members->NumElements(); i++) {
        FnDecl* fn = dynamic_cast<FnDecl*>(interface->members->Nth(i));
        if (!fn) continue;
        if (!astTable->definedNotGlobal(fn->id->name)) {
            if (!reportedNotImplemented) 
                for (int j = 0; j < implements->NumElements(); j++) {
                    if (strcmp(implements->Nth(j)->id->name, interface->id->name) == 0) {
                        ReportError::InterfaceNotImplemented(this, implements->Nth(j));
                    }
                }
            reportedNotImplemented = true;
        } else {
            FnDecl* myFn = dynamic_cast<FnDecl*>(astTable->get(fn->id->name)); 
            if(!fn->Equals(myFn)) {
                ReportError::OverrideMismatch(myFn);
            }
        }
        
    }
}

bool ClassDecl::SonOf(const char *other) {
    Hashtable<Node*>* current = astTable;
    while (current->parent != 0) {
        ClassDecl* currentClass = dynamic_cast<ClassDecl*>(current->get("this"));
        if (currentClass) {
            if (currentClass->extends && strcmp(currentClass->extends->id->name, other) == 0) { // if current class name matches other
                return true;
            }
            for (int i = 0; i < currentClass->implements->NumElements(); i++) {
                if (strcmp(currentClass->implements->Nth(i)->id->name, other) == 0) { //if an interface name matches other
                    return true;
                }
            }
        }
        current = current->parent;
    }
    return false;
}

InterfaceDecl::InterfaceDecl(Identifier *n, List<Decl*> *m) : Decl(n) {
    Assert(n != NULL && m != NULL);
    (members=m)->SetParentAll(this);
}

void InterfaceDecl::Check(Hashtable<Node*> *table, int pass) {
    if (pass == 0) {
        bool inserted = table->stMap.insert(std::make_pair(id->name, this)).second; 
        if (!inserted) {
            ReportError::DeclConflict(this, dynamic_cast<Decl*>(table->stMap[id->name]));
        }
        Hashtable<Node*> *classTable = new Hashtable<Node*>(table);
        astTable = classTable;

        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(classTable, pass);
        }
    } else if (pass == 1) {
        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(table, pass);
        }
    } else if (pass == 2) {
        for (int i = 0; i < members->NumElements(); i++) {
            members->Nth(i)->Check(table, pass);
        }
    }
}

FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl*> *d) : Decl(n) {
    Assert(n != NULL && r!= NULL && d != NULL);
    (returnType=r)->SetParent(this);
    (formals=d)->SetParentAll(this);
    body = NULL;
}

void FnDecl::Check(Hashtable<Node*> *table, int pass) {
    if (pass == 0) {
        Hashtable<Node*> *fnTable = new Hashtable<Node*>(table);
        astTable = fnTable;
        bool inserted = table->stMap.insert(std::make_pair(id->name, this)).second; 
        if (!inserted) {
            ReportError::DeclConflict(this, dynamic_cast<Decl*>(table->stMap[id->name])); 
        }
        
        //add return keyword for future return type checks
        fnTable->stMap.insert(std::make_pair("return", returnType));

        //add formals to current table 
        for (int i = 0; i < formals->NumElements(); i++) {
            formals->Nth(i)->Check(fnTable, pass);
        } 
        if (body) {
            body->Check(fnTable, pass);
        }
    } else if (pass == 2) {
        if (returnType->named) {
            NamedType* nt = dynamic_cast<NamedType*>(returnType);
            if (!astTable->defined(nt->id->name)) {
                ReportError::IdentifierNotDeclared(nt->id, LookingForType); 
            } 
        }
        for (int i = 0; i < formals->NumElements(); i++) {
            formals->Nth(i)->Check(table, pass);
        } 
        if (body) {
            body->Check(table, pass);
        }
    }
}

bool FnDecl::Equals(FnDecl* other) {
    if (!this->returnType->IsEquivalentTo(other->returnType)) {
        return false;
    }
    if (this->formals->NumElements() != other->formals->NumElements()) {
        return false;
    }
    for (int j = 0; j < this->formals->NumElements(); j++) {
        if (!this->formals->Nth(j)->type->IsEquivalentTo(other->formals->Nth(j)->type)) {
            return false;
        }
    }
    return true;
}

void FnDecl::SetFunctionBody(Stmt *b) { 
    (body=b)->SetParent(this);
}
