/* File: parser.y
 * --------------
 * Yacc input file to generate the parser for the compiler.
 *
 * pp2: your job is to write a parser that will construct the parse tree
 *      and if no parse errors were found, print it.  The parser should 
 *      accept the language as described in specification, and as augmented 
 *      in the pp2 handout.
 */

%{

/* Just like lex, the text within this first region delimited by %{ and %}
 * is assumed to be C/C++ code and will be copied verbatim to the y.tab.c
 * file ahead of the definitions of the yyparse() function. Add other header
 * file inclusions or C++ variable declarations/prototypes that are needed
 * by your code here.
 */
#include "scanner.h" // for yylex
#include "parser.h"
#include "errors.h"
#include "ast.h"
#include "hashtable.h"
#include <iostream>
#include <string>

void yyerror(const char *msg); // standard error-handling routine

%}

/* The section before the first %% is the Definitions section of the yacc
 * input file. Here is where you declare tokens and types, add precedence
 * and associativity options, and so on.
 */
 
/* yylval 
 * ------
 * Here we define the type of the yylval global variable that is used by
 * the scanner to store attibute information about the token just scanned
 * and thus communicate that information to the parser. 
 *
 * pp2: You will need to add new fields to this union as you add different 
 *      attributes to your non-terminal symbols.
 */
%union {
	int integerConstant;
	bool boolConstant;
	char *stringConstant;
	double doubleConstant;
	char identifier[MaxIdentLen+1]; // +1 for terminating null
	Decl *decl;
	List<Decl*> *declList;
	List<Expr*> *exprList;
	VarDecl *variableDecl;
	Type *type;
	FnDecl *functionDecl;
	Stmt *stmt;
	List<VarDecl*> *formals;
	List<NamedType*> *namedTypeList;
	StmtBlock* stmtBlock;
	List<Stmt*> *stmtList;
	Expr *expr;
	IntConstant *intConst;
}


/* Tokens
 * ------
 * Here we tell yacc about all the token types that we are using.
 * Yacc will assign unique numbers to these and export the #define
 * in the generated y.tab.h header file.
 */
%token   T_Void T_Bool T_Int T_Double T_String T_Class 
%token   T_Dims
%token   T_Null T_Extends T_This T_Interface T_Implements
%token   T_While T_For T_If T_Return T_Break
%token   <identifier> T_Identifier
%token   <stringConstant> T_StringConstant 
%token   <integerConstant> T_IntConstant
%token   <doubleConstant> T_DoubleConstant
%token   <boolConstant> T_BoolConstant

%nonassoc No_Else
%nonassoc T_Else
%right '='
%left T_Or
%left T_And
%left T_NotEqual T_Equal 
%left '<' '>' T_LessEqual T_GreaterEqual 
%left '-' '+'
%left '*' '/' '%'
%right T_New T_NewArray
%right '!'
%left T_ReadInteger T_ReadLine T_Print '.' '['


/* Non-terminal types
 * ------------------
 * In order for yacc to assign/access the correct field of $$, $1, we
 * must to declare which field is appropriate for the non-terminal.
 * As an example, this first type declaration establishes that the DeclList
 * non-terminal uses the field named "declList" in the yylval union. This
 * means that when we are setting $$ for a reduction for DeclList ore reading
 * $n which corresponds to a DeclList nonterminal we are accessing the field
 * of the union named "declList" which is of type List<Decl*>.
 * pp2: You'll need to add many of these of your own.
 */
%type <declList> DeclList 
%type <declList> PrototypeList 
%type <declList> FieldList
%type <decl> Decl
%type <decl> InterfaceDecl
%type <decl> ClassDecl
%type <decl> Prototype
%type <decl> Field
%type <stmt> Stmt
%type <stmt> BreakStmt
%type <stmtList> StmtList
%type <exprList> Actuals
%type <variableDecl> VariableDecl
%type <variableDecl> Variable
%type <functionDecl> FunctionDecl
%type <type> Type
%type <formals> Formals
%type <formals> FormalsList
%type <formals> VarDeclList
%type <stmtBlock> StmtBlock
%type <expr> OptionalExpr
%type <expr> Expr
%type <stmt> ForStmt
%type <stmt> IfStmt
%type <stmt> WhileStmt
%type <stmt> ReturnStmt
%type <stmt> PrintStmt
%type <expr> Constant
%type <expr> LValue
%type <expr> Call
%type <namedTypeList> IdentList
%type <namedTypeList> OptionalInter
%%
/* Rules
 * -----
 * All productions and actions should be placed between the start and stop
 * %% markers which delimit the Rules section.
	 
 */
Program		:	DeclList	{ 
						Program *program = new Program($1);
                        Hashtable<Node*> *global = new Hashtable<Node*>(0);
						// if no errors, advance to next phase
						if (ReportError::NumErrors() == 0) {
							program->Check(global, 0);
							program->Check(global, 1);
							program->Check(global, 2);
                        }
					}
		;

DeclList	:	DeclList Decl	{ ($$=$1)->Append($2); }
		|	Decl	{ ($$ = new List<Decl*>)->Append($1); }
		;

Decl		:	VariableDecl { $$ = $1; }
		|	FunctionDecl { $$ = $1; }
		|	InterfaceDecl { $$ = $1; }
		|	ClassDecl { $$ = $1; }
		;

VarDeclList	:	VariableDecl { ($$ = new List<VarDecl*>)->Append($1); }
		|	VarDeclList VariableDecl { ($$=$1)->Append($2); }
		;

VariableDecl	:	Type T_Identifier ';'	{ $$ = new VarDecl(new Identifier(@2, $2), $1); }
		;

Variable	:	Type T_Identifier { $$ = new VarDecl(new Identifier(@2, $2), $1); }
		;

Type		:	T_Int	{ $$ = Type::intType; }
		|	T_Double { $$ = Type::doubleType; }
		|	T_Bool { $$ = Type::boolType; }
		|	T_String { $$ = Type::stringType; }
		|	T_Identifier { $$ = new NamedType(new Identifier(@1, $1)); }
		|	Type T_Dims { $$ = new ArrayType(@1, $1); }
		;

FunctionDecl	:	Type T_Identifier '(' Formals ')' StmtBlock { $$ = new FnDecl(new Identifier(@2, $2), $1, $4); $$->SetFunctionBody($6); }
		|	T_Void T_Identifier '(' Formals ')' StmtBlock { $$ = new FnDecl(new Identifier(@2, $2), Type::voidType, $4); $$->SetFunctionBody($6); }
		;

Formals		:	/* empty */ { $$ = new List<VarDecl*>(); }
		|	FormalsList { $$ = $1; }
		;

FormalsList	:	FormalsList ',' Variable { ($$=$1)->Append($3); }
		|	Variable { ($$ = new List<VarDecl*>)->Append($1); }
		;

StmtBlock	:	'{' '}' { $$ = new StmtBlock(new List<VarDecl*>(), new List<Stmt*>()); }
		|	'{' StmtList '}' { $$ = new StmtBlock(new List<VarDecl*>(), $2); }
		|	'{' VarDeclList '}' { $$ = new StmtBlock($2, new List<Stmt*>()); }
		|	'{' VarDeclList StmtList '}' { $$ = new StmtBlock($2, $3); }
		;

StmtList	:	Stmt { ($$ = new List<Stmt*>)->Append($1); }
		|	StmtList Stmt { ($$=$1)->Append($2); }
		;

Stmt		:	OptionalExpr ';' { $$ = $1; }
		|	IfStmt { $$ = $1; }
		|	WhileStmt { $$ = $1; }
		|	ForStmt { $$ = $1; }
		|	BreakStmt { $$ = $1; }
		|	ReturnStmt { $$ = $1; }
		|	PrintStmt { $$ = $1; }
		|	StmtBlock { $$ = $1; }
		;

Expr		:	LValue '=' Expr { $$ = new AssignExpr($1, new Operator(@2, "="), $3); }
		|	Constant { $$ = $1; }
		|	LValue { $$ = $1; }
		|	T_This { $$ = new This(@1); }
		|	Call { $$ = $1; }
		|	'(' Expr ')' { $$ = $2; }
		|	Expr '+' Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "+"), $3); }
		|	Expr '-' Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "-"), $3); }
		|	Expr '*' Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "*"), $3); }
		|	Expr '/' Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "/"), $3); }
		|	Expr '%' Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "%"), $3); }
		|	'-' Expr { $$ = new ArithmeticExpr(new Operator(@1, "-"), $2); }
		|	Expr '<' Expr { $$ = new RelationalExpr($1, new Operator(@2, "<"), $3); }
		|	Expr T_LessEqual Expr { $$ = new RelationalExpr($1, new Operator(@2, "<="), $3); }
		|	Expr '>' Expr { $$ = new RelationalExpr($1, new Operator(@2, ">"), $3); }
		|	Expr T_GreaterEqual Expr { $$ = new RelationalExpr($1, new Operator(@2, ">="), $3); }
		|	Expr T_Equal Expr { $$ = new EqualityExpr($1, new Operator(@2, "=="), $3); }
		|	Expr T_NotEqual Expr { $$ = new EqualityExpr($1, new Operator(@2, "!="), $3); }
		|	Expr T_And Expr { $$ = new LogicalExpr($1, new Operator(@2, "&&"), $3); }
		|	Expr T_Or Expr { $$ = new LogicalExpr($1, new Operator(@2, "||"), $3); }
		|	'!' Expr { $$ = new LogicalExpr(new Operator(@1, "!"), $2); }
		|	T_ReadInteger '(' ')' { $$ = new ReadIntegerExpr(@$); }
		|	T_ReadLine '(' ')' { $$ = new ReadLineExpr(@$); }
		|	T_New '(' T_Identifier ')' { $$ = new NewExpr(@$, new NamedType(new Identifier(@3, $3))); }
		|	T_NewArray '(' Expr ',' Type ')' { $$ = new NewArrayExpr(@$, $3, $5); }
		;

LValue		:	T_Identifier { $$ = new FieldAccess(NULL, new Identifier(@1, $1)); }
		|	Expr '.' T_Identifier { $$ = new FieldAccess($1, new Identifier(@3, $3)); }
		|	Expr '[' Expr ']' { $$ = new ArrayAccess(@$, $1, $3); }
		;

Call		:	T_Identifier '(' Actuals ')' { $$ = new Call(@$, NULL, new Identifier(@1, $1), $3); }
		|	T_Identifier '(' ')' { $$ = new Call(@$, NULL, new Identifier(@1, $1), new List<Expr*>()); }
		|	Expr '.' T_Identifier '(' Actuals ')' { $$ = new Call(@$, $1, new Identifier(@3, $3), $5); }
		|	Expr '.' T_Identifier '(' ')' { $$ = new Call(@$, $1, new Identifier(@3, $3), new List<Expr*>()); }
		;

Actuals		:	Actuals ',' Expr { ($$=$1)->Append($3); }
		|	Expr { ($$ = new List<Expr*>())->Append($1); }
		;

Constant	:	T_IntConstant { $$ = new IntConstant(@1, yylval.integerConstant); }
		|	T_DoubleConstant { $$ = new DoubleConstant(@1, yylval.doubleConstant); }
		|	T_BoolConstant { $$ = new BoolConstant(@1, yylval.boolConstant); }
		|	T_StringConstant { $$ = new StringConstant(@1, yylval.stringConstant); }
		|	T_Null { $$ = new NullConstant(@1); }
		;

IfStmt		:	T_If '(' Expr ')' Stmt %prec No_Else { $$ = new IfStmt($3, $5, NULL); }
		|	T_If '(' Expr ')' Stmt T_Else Stmt { $$ = new IfStmt($3, $5, $7); }
		;

WhileStmt	:	T_While '(' Expr ')' Stmt { $$ = new WhileStmt($3, $5); }
		;

ForStmt		:	T_For '(' OptionalExpr ';' Expr ';' OptionalExpr ')' Stmt { $$ = new ForStmt($3, $5, $7, $9); }
		;

OptionalExpr	:	/* empty */ { $$ = new EmptyExpr(); }
		|	Expr { $$ = $1; }
		;

ReturnStmt	:	T_Return OptionalExpr ';' { $$ = new ReturnStmt(@2, $2); }
		;

BreakStmt	:	T_Break ';' { $$ = new BreakStmt(@1); }
		;

PrintStmt	:	T_Print '(' Actuals ')' ';' { $$ = new PrintStmt($3); }
		;

InterfaceDecl	:	T_Interface T_Identifier '{' PrototypeList '}' { $$ = new InterfaceDecl(new Identifier(@2, $2), $4); }
		|	T_Interface T_Identifier '{' '}' { $$ = new InterfaceDecl(new Identifier(@2, $2), new List<Decl*>()); }
		;

PrototypeList	:	PrototypeList Prototype { ($$=$1)->Append($2); }
		|	Prototype { ($$ = new List<Decl*>())->Append($1); }
		;

Prototype	:	Type T_Identifier '(' Formals ')' ';' { $$ = new FnDecl(new Identifier(@2, $2), $1, $4); }
		|	T_Void T_Identifier '(' Formals ')' ';' { $$ = new FnDecl(new Identifier(@2, $2), Type::voidType, $4); }
		;

ClassDecl	:	T_Class T_Identifier OptionalInter '{' FieldList '}' { $$ = new ClassDecl(new Identifier(@2, $2), NULL, $3, $5); 
                                                                             }

		|	T_Class T_Identifier OptionalInter '{' '}' { $$ = new ClassDecl(new Identifier(@2, $2), NULL, $3, new List<Decl*>()); 
                                                                   }

		|	T_Class T_Identifier T_Extends T_Identifier OptionalInter '{' FieldList '}' { $$ = new ClassDecl(new Identifier(@2, $2), new NamedType(new Identifier(@4, $4)), $5, $7); 
                                                                                                    }

		|	T_Class T_Identifier T_Extends T_Identifier OptionalInter '{' '}' { $$ = new ClassDecl(new Identifier(@2, $2), new NamedType(new Identifier(@4, $4)), $5, new List<Decl*>()); 
                                                                                          }
		;

OptionalInter	:	/* empty */ { $$ = new List<NamedType*>(); }
		|	T_Implements IdentList { $$ = $2; }
		;

IdentList	:	T_Identifier { ($$ = new List<NamedType*>())->Append(new NamedType(new Identifier(@1, $1))); }
		|	IdentList ',' T_Identifier { ($$=$1)->Append(new NamedType(new Identifier(@3, $3))); }
		;

FieldList	:	FieldList Field { ($$=$1)->Append($2); }
		|	Field { ($$ = new List<Decl*>())->Append($1); }
		;

Field		:	VariableDecl { $$ = $1; }
		|	FunctionDecl { $$ = $1; }
		;





%%

/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated y.tab.c file.
 * This section is where you put definitions of helper functions.
 */

/* Function: InitParser
 * --------------------
 * This function will be called before any calls to yyparse().  It is designed
 * to give you an opportunity to do anything that must be done to initialize
 * the parser (set global variables, configure starting state, etc.). One
 * thing it already does for you is assign the value of the global variable
 * yydebug that controls whether yacc prints debugging information about
 * parser actions (shift/reduce) and contents of state stack during parser.
 * If set to false, no information is printed. Setting it to true will give
 * you a running trail that might be helpful when debugging your parser.
 * Please be sure the variable is set to false when submitting your final
 * version.
 */
void InitParser()
{
   PrintDebug("parser", "Initializing parser");
   yydebug = false;
}



